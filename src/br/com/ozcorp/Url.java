package br.com.ozcorp;

import br.com.ozcorp.Funcionario;
import br.com.ozcorp.Sangue;
import br.com.ozcorp.Sexo;
import br.com.ozcorp.Cargo;
import br.com.ozcorp.Departamento;
import br.com.ozcorp.Titulo;

public class Url{

	public static void main(String[] args) {
		Funcionario cc = new Funcionario("Victor", "51898000X", 123659865, 23658, "lucasnarutinho@gmail.com", new Cargo(Titulo.DIRETOR, 937.50, "FN", 0, Departamento.FINANCEIRO), "123", Sexo.MASCULINO, Sangue.ABMAIS);
		
		//dados do funcionário
		System.out.println("Dados do Funcionário:");
		System.out.println("Nome:             " + cc.getNome());
		System.out.println("CPF:              " + cc.getCpf());
		System.out.println("RG:               " + cc.getRg());
		System.out.println("Mátricula:        " + cc.getMatricula());
		System.out.println("E-mail:           " + cc.getEmail());
		System.out.println("Senha:            " + cc.getSenha());
		System.out.print("Cargo:            " + cc.getCargo().getTitulo().t);
		System.out.println(" " + cc.getCargo().getDepartamento().d);
		System.out.println("Salário:          R$" + cc.getCargo().getSalario());
		System.out.println("Nível de Acesso:  " + cc.getCargo().getNivel());
		System.out.println("Sigla:            " + cc.getCargo().getSigla());
		System.out.println("Tipo Sanguíneo:   " + cc.getSangue().sangue);
		System.out.println("Sexo:             " + cc.getSexo().s);
		}
}