package br.com.ozcorp;

public enum Titulo {
	DIRETOR("Diretor"), SECRETARIO("Secretário"), GERENTE("Gerente"), ENGENHEIRO("Engenheiro"), ANALISTA("Analista");
	
	public String t;
	
	Titulo(String t){
		this.t = t;
	}
}
