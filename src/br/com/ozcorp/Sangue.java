package br.com.ozcorp;

public enum Sangue {
AMAIS("A+"),AMENOS("A-"),BMAIS("B+"),BMENOS("B-"),ABMAIS("AB+"),
ABMENOS("AB-"),OMAIS("O+"),OMENOS("O-");

	public String sangue;
	Sangue(String sangue){
		this.sangue = sangue;
	}
}
