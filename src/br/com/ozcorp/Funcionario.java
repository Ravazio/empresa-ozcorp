package br.com.ozcorp;

import br.com.ozcorp.Sexo;
import br.com.ozcorp.Sangue;
import br.com.ozcorp.Cargo;

public class Funcionario {
	
	//atributos
	private String nome;
	private String rg;
	private int cpf;
	private int matricula;
	private String email;
	private Cargo cargo;
	private String senha;
	private Sexo sexo;
	private Sangue sangue;
	
	public Funcionario(String nome, String rg, int cpf, int matricula, String email, Cargo cargo, String senha,
			Sexo sexo, Sangue sangue) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.cargo = cargo;
		this.senha = senha;
		this.sexo = sexo;
		this.sangue = sangue;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public Sangue getSangue() {
		return sangue;
	}
	public void setSangue(Sangue sangue) {
		this.sangue = sangue;
	}
}