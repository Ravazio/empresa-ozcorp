package br.com.ozcorp;

public class Cargo {
	private Titulo titulo;
	private double salario;
	private String sigla;
	private int nivel;
	private Departamento departamento;
	
	public Cargo(Titulo titulo, double salario, String sigla, int nivel, Departamento departamento) {
		super();
		this.titulo = titulo;
		this.salario = salario;
		this.sigla = sigla;
		this.nivel = nivel;
		this.departamento = departamento;
	}
	public Titulo getTitulo() {
		return titulo;
	}
	public void setTitulo(Titulo titulo) {
		this.titulo = titulo;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}	
}